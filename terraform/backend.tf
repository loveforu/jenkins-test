terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-hy"
    key = "jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}
